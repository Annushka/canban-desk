function insertCardMaker(element) {
    let column = getElementColumn(element);

    let cardMakerElement = document.createElement('div');
    cardMakerElement.className = 'card-maker';
    cardMakerElement.innerHTML = cardMakerInnerHTML;

    column.querySelector('.add').remove();

    if(column.querySelector('.cards-wrapper').children.length === 0) {
        column.querySelector('.column-title').style.marginBottom = '12px';
    }

    column
        .querySelector('.cards-wrapper')
        .insertAdjacentElement('beforeend', cardMakerElement);

    column.querySelector('.cards-wrapper').scrollTop = column.querySelector('.cards-wrapper').scrollHeight;
}


function deleteCardMaker(element) {
    let column = getElementColumn(element);

    let addCardElement = document.createElement('div');
    addCardElement.className = 'add';
    addCardElement.innerHTML = addCardInnerHTML;

    column.querySelector('.card-maker').remove();
    if(column.querySelector('.cards-wrapper').children.length === 0) {
        column.querySelector('.column-title').style.marginBottom = '0';
    }

    column.insertAdjacentElement('beforeend', addCardElement);
}


function insertCard(element) {
    let column = getElementColumn(element);
    let card = document.createElement('div');

    card.className = 'card';
    card.innerHTML = getCardText(column);

    column.querySelector('.cards-wrapper').insertAdjacentElement('beforeend', card);
    deleteCardMaker(column);
}

function getCardText(column) {
    return column.querySelector('textarea').value;
}


function getElementColumn(element) {
    while(element && !element.classList.contains('column')) {
        element = element.parentElement;
    }
    return element;
}

const cardMakerInnerHTML = "<textarea oninput='textareaAutoResize(this)' rows=2 placeholder='Введите название карточки'></textarea>" +
    "            <div class='toolbar-adding'>" +
    "                <button class='submit-adding' onclick='insertCard(this)'>Добавить карточку</button>" +
    "                <button class='delete-button' onclick='deleteCardMaker(this)'></button>" +
    "            </div>";

const addCardInnerHTML =
    "<button class='add-button' onclick='insertCardMaker(this)'></button>" +
    "        <div>Добавить еще одну карточку</div>";