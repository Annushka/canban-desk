const columnMakerHTML =
    "<div class='column column-maker'>" +
    "        <input class='card' placeholder='Введите название колонки'>" +
    "        <div class='toolbar-adding'>" +
    "            <button class='submit-adding' onclick='insertEmptyColumn()'>Добавить колонку</button>" +
    "            <button class='delete-button' onclick='deleteColumnMaker(this)'></button>" +
    "        </div>" +
    "</div>";

function insertColumnMaker () {
    if(document.getElementsByClassName('column-maker').length > 0)
        return;

    document
        .querySelector('.add-column')
        .insertAdjacentHTML('beforebegin', columnMakerHTML)
}

function deleteColumnMaker(deleteButton) {
    let columnMaker = deleteButton;

    while(columnMaker && !columnMaker.classList.contains('column-maker')) {
        columnMaker = columnMaker.parentElement;
    }
    columnMaker.remove();
}