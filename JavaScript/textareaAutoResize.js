function textareaAutoResize(textarea) {
  while(textarea.scrollHeight > textarea.clientHeight)
    textarea.rows++;

  if(textarea.rows === 2)
    return;

  while(textarea.scrollHeight === textarea.clientHeight) {
    textarea.rows--;
    if (textarea.scrollHeight > textarea.clientHeight) {
      textarea.rows++;
      break;
    }
  }
}
