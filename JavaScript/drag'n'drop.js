let dragMaster = (function() {

	let dragObject, mouseOffset;

	function mouseDown(event) {
		if (event.button !== 0) return;

		dragObject  = event.target;
		if(!dragObject.classList.contains('card') || dragObject.tagName !== 'DIV')
		    return;

		mouseOffset = getMouseOffset(dragObject, event);
		saveOldPosition();

		document.onmousemove = mouseMove;
		document.onmouseup = mouseUp;
		document.ondragstart = () => false;
		document.body.onselectstart = () => false;
	}

	function mouseMove(event){
	    dragObject.style.position = 'absolute';
	    dragObject.style.top = event.pageY - mouseOffset.y + 'px';
		dragObject.style.left = event.pageX - mouseOffset.x + 'px';
		dragObject.style.zIndex = 9999;
		if(dragObject.old.column.querySelector('.cards-wrapper').children.length === 1) {
		    dragObject.old.column.querySelector('.column-title').style.marginBottom = '0';
		}
	}

	function mouseUp(event){
	    dragObject.hidden = true;
	    let column = findColumnToDrop(event);
	    if (!column) {
	        dragObject.rollback();
	        finishMouseUp();
	        return;
	    }
	    if (column.querySelector('.cards-wrapper').children.length === 0) {
	        column.querySelector('.column-title').style.marginBottom = '12px';
	        column
                .querySelector('.cards-wrapper')
                .insertAdjacentElement('beforeend', dragObject);
	        finishMouseUp();
	        return;
	    }

	    if(dragObject.old.column === column && column.querySelector('.cards-wrapper').children.length === 1)
	        column.querySelector('.column-title').style.marginBottom = '12px';

	    let x = event.clientX, y = event.clientY;
	    let elementUnderMouse = document.elementFromPoint(x, y);
	    function updateElementUnderMouse() {
	        elementUnderMouse = document.elementFromPoint(x, y);
        }

	    if(!elementUnderMouse.classList.contains('cards-wrapper') && !elementUnderMouse.classList.contains('card')) {
	        y -= 40;
	        updateElementUnderMouse();
	        let cardsWrapper = column.querySelector('.cards-wrapper');
	        if(!elementUnderMouse || elementUnderMouse.tagName === 'BODY') {
	            cardsWrapper.insertAdjacentElement('afterbegin', dragObject);
            } else {
	            cardsWrapper.insertAdjacentElement('beforeend', dragObject);
            }
	        finishMouseUp();
	        return;
        }

	    if(elementUnderMouse.classList.contains('card')) {
	        function onCard() {
                    elementUnderMouse.insertAdjacentElement('afterend', dragObject);
            }
            onCard();
	        finishMouseUp();
	        return;
        }

	    if(elementUnderMouse.classList.contains('cards-wrapper')) {
	        x -= 13;
	        updateElementUnderMouse();
	        if(elementUnderMouse.classList.contains('card')) {
	            onCard();
	            finishMouseUp();
	            return;
            }
	        x += 26;
	        updateElementUnderMouse();
	        if(elementUnderMouse.classList.contains('card')) {
	            onCard();
	            finishMouseUp();
	            return;
            }
	        x -= 13;
	        y -= 9;
	        updateElementUnderMouse();
	        if(elementUnderMouse.classList.contains('card')) {
	            elementUnderMouse.insertAdjacentElement('afterend', dragObject);
	            finishMouseUp();
	            return;
            }
	        y += 9;
	        x += 13;
	        if(elementUnderMouse.classList.contains('card')) {
	            elementUnderMouse.insertAdjacentElement('afterend', dragObject);
	            finishMouseUp();
	            return;
            }
	        x -= 26;
	        if(elementUnderMouse.classList.contains('card')) {
	            elementUnderMouse.insertAdjacentElement('afterend', dragObject);
	            finishMouseUp();
	            return;
            }
        }

		function finishMouseUp() {
		    dragObject.hidden = false;
		    dragObject = null;
		    document.onmousemove = null;
		    document.onmouseup = null;
		    document.ondragstart = null;
		    document.body.onselectstart = null;
        }
	}

	function saveOldPosition() {
	    dragObject.old = {};
		dragObject.old.position = dragObject.style.position || 'static';
		dragObject.old.zIndex = dragObject.style.zIndex || 0;
		dragObject.old.column = getElementColumn(dragObject);

		if(dragObject.previousSibling) {
            dragObject.old.sibling = dragObject.previousSibling;
            dragObject.old.insertPosition = 'afterend';
        } else if(dragObject.nextSibling) {
		    dragObject.old.sibling = dragObject.nextSibling;
		    dragObject.old.insertPosition = 'beforebegin';
        } else {
		    dragObject.old.sibling = dragObject.parentElement;
		    dragObject.old.insertPosition = 'beforeend';
        }

		dragObject.rollback = function() {
		    dragObject.style.zIndex = dragObject.old.zIndex;
		    dragObject.style.position = dragObject.old.position;
		    dragObject.old.sibling.insertAdjacentElement(dragObject.old.insertPosition, dragObject);
		    if(dragObject.old.column.querySelector('.cards-wrapper').children.length === 1) {
	            dragObject.old.column.querySelector('.column-title').style.marginBottom = '12px';
	        }
        };
    }

    function findColumnToDrop(event) {
	        dragObject.hidden = true;
	        let elementUnderMouse = document.elementFromPoint(event.clientX, event.clientY);
	        dragObject.hidden = false;

	        if(elementUnderMouse && getElementColumn(elementUnderMouse)) {
	            dragObject.style.position = dragObject.old.position;
	            dragObject.style.zIndex = dragObject.old.zIndex;
	            return getElementColumn(elementUnderMouse);
	        }
	}

	function getMouseOffset(target, e) {
		return {
		    x: e.pageX - getPosition(target).x,
            y: e.pageY - getPosition(target).y
		}
	}

	return {
		makeDraggable: function(){
			document.onmousedown = mouseDown;
		}
	}

}());

function getPosition(e){
	let left = 0;
	let top  = 0;

	while (e.offsetParent){
		left += e.offsetLeft;
		top += e.offsetTop;
		e = e.offsetParent
	}

	left += e.offsetLeft;
	top  += e.offsetTop;

	return {
	    x:left,
        y:top
	};
}
