const getEmptyColumnInnerHTML = (columnName) =>
    "    <div class='column-title'>" +
             columnName +
    "    </div>" +
    "    <div class='cards-wrapper'></div>" +
    "    <div class='add'>" +
    "        <button class='add-button' onclick='insertCardMaker(this)'></button>" +
    "        <div>Добавить еще одну карточку</div>" +
    "    </div>";

const getNewColumnName =  () => document.getElementsByTagName('input')[0].value;

function insertEmptyColumn() {
    const emptyColumnElement = document.createElement('div');

    emptyColumnElement.setAttribute('class', 'column');
    emptyColumnElement.innerHTML = getEmptyColumnInnerHTML(getNewColumnName());
    document
        .querySelector('.column-maker')
        .replaceWith(emptyColumnElement);

    emptyColumnElement.querySelector('.cards-wrapper').style.maxHeight = 559 -
        emptyColumnElement.querySelector('.column-title').clientHeight + 'px';
}
